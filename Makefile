default:
	make vmd

dev:
	python spheres.py data/d1PEF.pdb

clean:
	rm out/*.tk -f
	rm out/*.in -f


# Examples

vmd:
	python3 spheres.py data/1PEF.pdb out/1PEF.in.tk --vmd
	vmd data/1PEF.pdb -e out/1PEF.in.tk

broken:
	python3 spheres.py data/2ZWB.pdb out/2ZWB.in.tk --vmd
	vmd data/2ZWB.pdb -e out/2ZWB.in.tk
	

larger:
	python3 spheres.py data/4Q4W.pdb out/4Q4W.in.tk --vmd
	vmd data/4Q4W.pdb -e out/4Q4W.in.tk

humongous:
	python3 spheres.py data/1BPV.pdb out/1BPV.in.tk --vmd
	vmd data/1BPV.pdb -e out/1BPV.in.tk

vis:
	@tput setaf 6
	# WARNING! lacking vtk I haven't tested this action, but it should work
	@tput sgr0
	python3 spheres.py data/2ZWB.pdb out/2ZWB.in --vis
	python2 bin/viscavity.py data/2ZWB.pdb out/2ZWB.in

