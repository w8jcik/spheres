# Running

Script generates output in two formats (for viscavity and vmd).
One of the switches has to be used (--vis or --vmd).

The output generated for vmd is a .tk script which has to be passed
along initial protein.

```  
  python3 spheres.py data/1PEF.pdb out/1PEF.in.tk --vmd
  vmd data/1PEF.pdb -e out/1PEF.in.tk
```

# Prerequisites

 * Python 3
 * numpy
 * scipy
 * psutil (optional: for profiling)


# Performance

Some optimizations could be performed, especially easy in terms
of memory consumption but a simple benchmark shows that they might
not be needed. 

```
  1PEF →   162 atoms → finished after   1.17 s, script   1.16 s, qvoronoi 0.02 s, memory consumed  10.8 MB
  2ZWB →  1461 atoms → finished after   6.89 s, script   6.72 s, qvoronoi 0.16 s, memory consumed  17.6 MB
  4Q4W →  7367 atoms → finished after  32.10 s, script  31.16 s, qvoronoi 0.93 s, memory consumed  38.6 MB
  1BPV → 40350 atoms → finished after 169.39 s, script 164.67 s, qvoronoi 4.72 s, memory consumed 184.9 MB
```

(CPU: Core 2 Duo ULV @ 1.4 GHz)

One of the most obvious improvements would be to do the computations 
on all cores of the cpu in parallel.


# Handling troublesome PDB files

Simple error handling is being provided by PDBParser with verbose 
messages appearing in the terminal.

More severe problems require an additional logic for exception handling
inside the script or some preprocessing of input data.


# Issues

VMD doesn't respect "display udpate off" command while adding spheres.
As a result, process of drawing takes more time than it should.
