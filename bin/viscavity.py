#!/usr/bin/python

from sys import argv
import vtk
import numpy

if len(argv) != 3:
	print "Usage:\n%s  file.pdb  spheres.dat" % argv[0]
	exit(1)

pdb = vtk.vtkPDBReader()
pdb.SetFileName(argv[1])
pdb.SetHBScale(1.0)
pdb.SetBScale(1.0)
pdb.Update()

tube = vtk.vtkTubeFilter()
tube.SetInputConnection(pdb.GetOutputPort())
tube.SetNumberOfSides(10)
tube.SetRadius(0.2)
tube.SetVaryRadius(0)
tube.SetRadiusFactor(10)

tubemap = vtk.vtkPolyDataMapper()
tubemap.SetInputConnection(tube.GetOutputPort())
tubemap.ImmediateModeRenderingOn()
tubemap.UseLookupTableScalarRangeOff()
tubemap.SetScalarModeToDefault()
tubemap.ScalarVisibilityOff()

lodactor = vtk.vtkLODActor()
lodactor.SetMapper(tubemap)
lodactor.GetProperty().SetColor(0.0, 1.0, 0.0)

sphfile = open(argv[2])
nspheres = int(sphfile.readline())

radii = vtk.vtkFloatArray()
radii.SetNumberOfComponents(1)
radii.SetNumberOfValues(nspheres)

sphere_centers = vtk.vtkPoints()
sphere_centers.SetNumberOfPoints(nspheres)
for i in xrange(nspheres):
	line = map(float, sphfile.readline().split())
	sphere_centers.InsertPoint(i, line[0], line[1], line[2])
	radii.SetValue(i, line[3]*1.8)

grid = vtk.vtkUnstructuredGrid()
grid.SetPoints(sphere_centers)
grid.GetPointData().SetScalars(radii)

sphere = vtk.vtkSphereSource()
sphere.SetThetaResolution(16)
sphere.SetPhiResolution(12)

glyph = vtk.vtkGlyph3D()
glyph.SetInputData(grid)
glyph.ScalingOn()
glyph.SetScaleModeToScaleByScalar()
glyph.SetSourceConnection(sphere.GetOutputPort())

gridmap = vtk.vtkPolyDataMapper()
gridmap.SetInputConnection(glyph.GetOutputPort())

gridactor = vtk.vtkLODActor()
gridactor.SetMapper(gridmap)
gridactor.GetProperty().SetColor(1.0, 0.0, 0.0)

renderer = vtk.vtkRenderer()
renderer.AddActor(lodactor)
renderer.AddActor(gridactor)
renderer.SetBackground(1.0, 1.0, 1.0)

window = vtk.vtkRenderWindow()
window.SetSize(600,600)
window.AddRenderer(renderer)

istyle = vtk.vtkInteractorStyleSwitch()
istyle.SetCurrentStyleToTrackballCamera()

interactor = vtk.vtkRenderWindowInteractor()
interactor.SetRenderWindow(window)
interactor.SetInteractorStyle(istyle)

interactor.Initialize()
interactor.Start()
