#!/usr/bin/python3

### Track the time and memory consumption

import time
elapsed_on_voronoi = 0.0
elapsed_start = time.clock()

enable_profiling = False

if enable_profiling:
  import psutil
  memory_used_at_start = psutil.phymem_usage().used


### Parse CLI arguments and options

from optparse import OptionParser

default_heavy_threshold = 1.5

parser = OptionParser("usage: %prog [options] in_file out_file")

parser.add_option('--vis', action="store_const", const='viscavity', dest='out_format', default='viscavity', help="save in viscavity format")
parser.add_option('--vmd', action='store_const', const='vmd', dest='out_format', help="save as vmd script")

parser.add_option("-t", "--heavy-threshold", metavar="FLOAT",
  dest="heavy_threshold", default=default_heavy_threshold, type="float", help="atoms with mass higher than threshold are considered heavy, default is %s u" % default_heavy_threshold)

(options, args) = parser.parse_args()

if len(args) != 2:
  parser.error("incorrect number of arguments")
  
pdb_filename = args[0]
out_filename = args[1]


### Biopython workarounds

# Workaround for a missing mass of deuterium
# other can be easily added.

import Bio.Data.IUPACData
Bio.Data.IUPACData.atom_weights.update({
  'D': 2.014102
})

from Bio.PDB import *


### Define data types

class Atom(Atom.Atom):
  vertices = []
  
class Point:
  coords = []
  
  def __init__(self, coords):
    self.coords = coords

  def __repr__(self):
    return "<%s %2.1f %2.1f %2.1f>" % tuple(self.__class__.__name__, self.coords)
    
class Vertex(Point):
  coords = []
  atoms = []
    
  def __repr__(self):
    return "<%s %2.1f %2.1f %2.1f>" % tuple(self.__class__.__name__, self.coords)

class Sphere:
  point = None
  radius = 0.0

  def __init__(self, point, radius):
    self.point = point
    self.radius = radius

  def __repr__(self):
    return "<%s %2.1f %2.1f %2.1f |%2.1f>" % tuple(self.__class__.__name__, self.point.coords, self.radius)


### The qvoronoi wrapper

def calculate_voronoi(pdb_filename):
  global elapsed_on_voronoi
  
  atoms = []
  vertices = []
  
  p = PDBParser()
  structure = p.get_structure('X', pdb_filename)

  for model in structure:
    for chain in model:
      for residue in chain:
        for atom in residue:
          if atom.mass > options.heavy_threshold:
            atoms.append(atom)

  print("%d atoms readed from %s" % (len(atoms), pdb_filename))

  import subprocess
  
  start = time.time()
  
  process = subprocess.Popen(['qvoronoi', 'p', 'FN'], stdin=subprocess.PIPE, stdout=subprocess.PIPE)

  qvoronoi_input = "%d\n%d\n%s\n" % (3, len(atoms),
    "\n".join(map(str, map(lambda atom: "%8.4f %8.4f %8.4f" % tuple(atom.coord), atoms)))
  )

  process.stdin.write(qvoronoi_input.encode('UTF-8'))
  process.stdin.close()
  answer = process.stdout.read().decode('UTF-8')
  process.stdin.close()
  
  elapsed_on_voronoi = time.time() - start

  lines = answer.split('\n')


  # Parse qvoronoi output

  start_reading = 2
  vertices_number = int(lines[start_reading-1])

  def line_to_vertex(line):
    return Vertex(list(map(float, line.split())))

  vertices = list(map(line_to_vertex, lines[start_reading:(vertices_number+start_reading)]))

  print("%d vertices found" % (vertices_number))

  start_reading = start_reading + vertices_number + 1
  atoms_number = int(lines[start_reading - 1])

  i = 0
  for line in lines[start_reading:-1]:
    atoms[i].vertices = set(map(lambda i: vertices[i], filter(lambda number: number >= 0, map(int, line.split()[1:]))))
    i += 1


  # Make vertex → atoms relation.

  for i in range(0, len(vertices)):
    vertices[i].atoms = set()

  for atom in atoms:
    for vertex in atom.vertices:
      vertex.atoms.add(atom)
  
  
  # Don't need atom → vertices relation anymore,
  # so give garbage collector a hint.

  for atom in atoms:
    atom.vertices = []
  
  return (atoms, vertices)


### Procedure detecting spheres

def calculate_spheres(atoms, vertices):
  distances = {}

  for vertex in vertices:
    distances[vertex] = {}
    
  from scipy.spatial import distance
  
  for vertex in vertices:
    for atom in vertex.atoms:
      distances[vertex][atom] = distance.euclidean(atom.coord, vertex.coords)

  import numpy

  spheres = []
  for vertex in vertices:
    
    closest = min(map(lambda atom: distances[vertex][atom], vertex.atoms))
    
    product_vector = numpy.array([0, 0, 0])
    for atom in vertex.atoms:
      product_vector += (numpy.array(atom.coord) - numpy.array(vertex.coords))
    
    product_distance = distance.euclidean((0, 0, 0,), product_vector)
    
    if closest > 3 and product_distance <= closest:
      spheres.append(Sphere(point=vertex, radius=closest))
    
  print("%d sphere[s] detected" % len(spheres))
  
  return spheres


### Procedures saving the results (in two possible formats)

def save_for_vmd(spheres):
  print("saving to %s" % out_filename)
  
  out_file = open(out_filename, 'w')

  print('''
axes location Off
draw color magenta2
mol modcolor 0 0 ResType
draw material Transparent
display update off''', file=out_file)

  for sphere in spheres:
    print("draw sphere {%f %f %f}" % tuple(sphere.point.coords), file=out_file, end=' ')
    print("radius %f" % (sphere.radius), file=out_file)

  print('''
mol modstyle 0 0 Surf 1.400000 0.000000
display update on''', file=out_file)

def save_for_viscavity(spheres):
  out_file = open(out_filename, 'w')
  
  print("%d" % len(spheres), file=out_file)
  
  for sphere in spheres:
    print("%8.4f %8.4f %8.4f" % tuple(map(float, sphere.point.coords)), file=out_file, end='')
    print("%8.4f" % sphere.radius, file=out_file)



### Run

(atoms, vertices) = calculate_voronoi(pdb_filename)
spheres = calculate_spheres(atoms, vertices)

print("format %s" % options.out_format)

if options.out_format == 'vmd':
  save_for_vmd(spheres)

elif options.out_format == 'viscavity':
  save_for_viscavity(spheres)


### Print the time and memory consumption

elapsed = time.clock() - elapsed_start

print("finished after %.2f s, script %.2f s, qvoronoi %.2f s" % (elapsed, elapsed - elapsed_on_voronoi, elapsed_on_voronoi))

if enable_profiling:
  print("memory consumed %.1f MB" % ((psutil.phymem_usage().used - memory_used_at_start)/1024/1024))
